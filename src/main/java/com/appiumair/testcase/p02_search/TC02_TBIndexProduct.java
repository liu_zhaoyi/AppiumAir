package com.appiumair.testcase.p02_search;

import com.appiumair.controller.TestCase;
import com.appiumair.report.ExtentReport;
import com.appiumair.util.AppiumUtils;
import io.appium.java_client.android.AndroidDriver;

import java.util.concurrent.TimeUnit;

/**
 * 测试用例：首页搜索
 * 如何编写一个标准的测试用例：继承ExtentReport类，实现TestCase接口，重写run方法
 */
public class TC02_TBIndexProduct extends ExtentReport implements TestCase {
    /**
     * @module 上下滑动浏览
     * @param driver
     * @throws InterruptedException
     */
    @Override
    public void run(AndroidDriver driver) throws InterruptedException {
        for (int i = 0; i < 1; i++) {
            //driver.navigate().back();
            TimeUnit.MILLISECONDS.sleep(500);
            AppiumUtils.swipeUp(driver);
            TimeUnit.MILLISECONDS.sleep(500);
            AppiumUtils.swipeUp(driver);
            TimeUnit.MILLISECONDS.sleep(500);
            AppiumUtils.swipeUp(driver);
            TimeUnit.MILLISECONDS.sleep(500);
            AppiumUtils.swipeUp(driver);
            TimeUnit.MILLISECONDS.sleep(500);
            TimeUnit.MILLISECONDS.sleep(500);
            AppiumUtils.swipeDown(driver);
            TimeUnit.MILLISECONDS.sleep(500);
            AppiumUtils.swipeDown(driver);
            TimeUnit.MILLISECONDS.sleep(500);
            AppiumUtils.swipeDown(driver);
            TimeUnit.MILLISECONDS.sleep(500);
            AppiumUtils.swipeDown(driver);
            TimeUnit.MILLISECONDS.sleep(500);
        }
    }
}
